# Donkeyno Subtitle Transcripts

This repository contains Donkeyno Subtitle Transcripts, translations from
subtitle strings found in kino files to Miir Concept translations. The
transcripts are to be saved in a YAML file, respecting the following structure.


    ---

    subtitle_transcripts:

        - original: <Original String here>
          transcript: <Translated String here>
