"""
This script can be used to create a diff between two
donkeyno-subtitle-transcript files
"""
import argparse
from yaml import load
from collections import namedtuple


Transcript = namedtuple('Transcript', 'original')


def get_args():
    description = "Get diff between two subtitle-transcript YAML Files"
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('left', type=str,
                        help="Left file (Already transcripted subtitles)")
    parser.add_argument('right', type=str, help="Right file")
    return parser.parse_args()


def load_subtitles(filename):
    result = []
    with open(filename, 'r') as filehandler:
        content = load(filehandler)

    for subtitle_transcript in content['subtitle_transcripts']:
        result.append(Transcript(subtitle_transcript['original']))
    return result


def print_diff(left, right):
    difference = list(set(right) - set(left))
    for entry in difference:
        print("  - original: %s" % entry.original)
        print("    transcript: __Not_transcripted_yet__")


if __name__ == "__main__":
    arguments = get_args()
    subs = load_subtitles(arguments.left)
    non_transcr_subs = load_subtitles(arguments.right)
    print_diff(subs, non_transcr_subs)
