"""
Validates if input file repects the Schema defined for subtitle transcript
file.
"""

import sys
import argparse
import logging
from yaml import load, BaseLoader
from schema import Schema, And


LOG = logging.getLogger(__name__)


def parse_args():
    description = "Validate if Schema for subtitle transcripts is respected"
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('file_to_validate', type=str)
    return parser.parse_args()


def validate(arguments):
    """
    Validate process
    """
    not_transcript = '__Not_transcripted_yet__'
    subtrans_schema = Schema(
        {
            'subtitle_transcripts': [
                {
                    'original': str,
                    'transcript': And(str, lambda x: x != not_transcript),
                }
            ]
        }
    )

    filename = arguments.file_to_validate
    filehandler = open(filename, 'r')

    LOG.info("Validating subtitle transcript file %r" % filename)
    content = load(filehandler, Loader=BaseLoader)

    if subtrans_schema.is_valid(content):
        LOG.info("Transcript file is valid")
        sys.exit(0)
    else:
        LOG.error("Transcript file is invalid")
        subtrans_schema.validate(content)
        sys.exit(1)


if __name__ == "__main__":
    # Setup logging
    logging.basicConfig(level=logging.INFO)

    # Start script
    arguments = parse_args()
    validate(arguments)
