"""
Script that extracts a unique set of MIIR subtitles from a given subtitle yaml
file

.. Usage::

    ./env/bin/python scripts/unique_subtitles.py my_yaml.yml
"""
import logging
import argparse
import glob
from os.path import join

from yaml import load


LOG = logging.getLogger(__name__)


def get_args():
    description = "Extract a unique set of MIIR subtitles from a yaml file"
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('yamlfile',
                        type=str,
                        help="Subtitle yaml file")
    return parser.parse_args()


def get_unique_miir_subtitles(args):
    """
    Opens up the yaml file specified in the args and returns a list of unique
    MIIR subtitles
    """
    result = []
    with open(args.yamlfile, 'r') as filehandler:

        content = load(filehandler)

        for transcript in content.get('subtitle_transcripts', []):
            result.append(transcript.get('transcript', '').strip())

    return list(set(result))


def output(subtitles):
    """
    Print results
    """
    for subtitle in subtitles:
        print(subtitle)


if __name__ == "__main__":
    arguments = get_args()
    result = get_unique_miir_subtitles(arguments)
    output(result)
