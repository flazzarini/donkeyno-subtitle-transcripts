"""
Script that can be used to compile a unique set of non transcripted subtitle
names using a set of kino files as input.

.. Usage::

    ./env/bin/python scripts/extract_subtitles.py folder/ output_file.yml
"""
import logging
import argparse
import glob
from os.path import join
from collections import namedtuple

from yaml import dump
from openpyxl import load_workbook


LOG = logging.getLogger(__name__)
Subtitle = namedtuple('Subtitle', 'original, transcript')
NotTranscripted = "__Not_transcripted_yet__"


def extract_subtitles(sheet):
    """
    Extracts subtitles from sheet
    """
    results = []
    cell_range = sheet["AA0":"AA600"]
    for cell in cell_range:
        subtitle_raw = cell.value

        if not subtitle_raw:
            continue

        subtitle = Subtitle(subtitle_raw.strip(), NotTranscripted)
        results.append(subtitle)
    return results


def process_files(filenames):
    """
    Processes each filename with openpyxl and delegates a call to extract
    subtitle instances
    """
    results = []
    for filename in filenames:
        LOG.info("Processing %r" % filename)
        wbook = load_workbook(filename)
        sheet = wbook.worksheets[0]
        subtitles = extract_subtitles(sheet)
        LOG.info("Found %r subtitles" % len(subtitles))
        results += subtitles
        LOG.info("Results has now %r subtitles" % len(results))

    results = list(set(results))
    LOG.info("After deduplication Results has now %r subtitles" % len(
        results))
    results.sort()
    return results


def get_files(arguments):
    """
    Get filenames that have a suffix of ".xls" contained in the folder passed
    via the arguments
    """
    folder = arguments.folder
    folder = join(folder, "**/*.xlsx")
    files = glob.glob(folder, recursive=True)
    LOG.info("Found %s files" % len(files))
    return files


def get_args():
    description = "Extract subtitles from xls files"
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('folder',
                        type=str,
                        help="Folder containing Scala XLS files")
    parser.add_argument('output',
                        type=str,
                        help="Write to this output file YAML")
    return parser.parse_args()


def write_to_yaml(subtitles, arguments):
    subs = []
    for subtitle in subtitles:
        subs.append(
            {
                'original': subtitle.original,
                'transcript': subtitle.transcript
            }
        )
    result = {'subtitle_transcripts': subs}
    with open(arguments.output, 'w') as outfile:
        dump(result, outfile, default_flow_style=False)
    LOG.info("Written non-transcripted subtitles to %s" % arguments.output)


if __name__ == "__main__":
    arguments = get_args()
    files = get_files(arguments)
    subs = process_files(files)
    write_to_yaml(subs, arguments)
